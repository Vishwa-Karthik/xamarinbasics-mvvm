# MVVM Model
A MVVM Concept Implied with ListView builder for Anime Profile Cards

## Results
<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-mvvm/-/raw/master/img1.png" width="200" height="400" /> &nbsp;

<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-mvvm/-/raw/master/img2.png" width="200" height="400" /> &nbsp;

<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-mvvm/-/raw/master/img3.png" width="200" height="400" /> 
</p>
