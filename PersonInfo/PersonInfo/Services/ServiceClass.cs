﻿using PersonInfo.model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonInfo.Services
{
    internal class ServiceClass
    {
        public List<Information> getInfo()
        {
            return new List<Information>()
            {
                new Information
                {
                    Name="Goku",
                    Description="The main protagonist and hero of the Dragon Ball manga series and animated television series created by Akira Toriyama. He is one of the survivors of the extinct Saiyan race. Sent as a baby to planet Earth in order to destroy it. When he arrived he was a violent kid, due to his warrior nature. However, he suffered an accident which made him lose his memory. He became a kind and calm kid. Trained, he became a talented martial artist and world's greatest defender",
                    imageUrl ="goku.png"
                },
                new Information
                {
                   Name="Vegeta",
                    Description="Vegeta, formerly the Prince of all Saiyans, was previously a supervillain who later evolved into an anti-hero and eventually a hero through the duration of the DragonBall franchise. An exceptional fighting prodigy, Vegeta has trained himself since a young age; and has always had natural talents in martial arts and chi control. He is among the last surviving pure Saiyans from the planet Vegeta. Vegeta's obsession to surpass Goku is among the key traits to his character.",
                    imageUrl ="vegata.png"
                },
                new Information
                {
                    Name="Gohan",
                    Description="The first son of Goku and Chichi and a half-Human, half-Saiyan, Son Gohan held the potential for enormous power. Growing up a scholar, not a warrior like his father, he nevertheless has answered the call to protect Earth multiple times. He is the most powerful non fused warrior in the Dragon Ball Z universe by the end of Dragon Ball Z series. He is the father of Pan and husband to Videl. He is Goten's older brother and Grandson to both Ox King and Bardock, respectively. (Goku and Chichi's fathers)",
                    imageUrl ="gohan.png"
                },
                new Information
                {
                    Name="Master Kai",
                    Description="King Kai is one of four guardians that protect the universe. The universe is separated into the north, south, east and west galaxies. King Kai is in charge of watching over the north galaxy. He taught Goku how to use the Kaio-ken technique as well as the Spirit Bomb.",
                    imageUrl ="kai.png"
                },
                new Information
                {
                    Name="Krilin",
                    Description="Krillin is the most powerful human in the Dragonball Universe. Krillin is short, bald and has six dots tattooed on his forehead. He usually wears the the turtle hermit school name on his uniform. Krillin is married and has a daughter. After the cell games, Krillin eventually grew hair.",
                    imageUrl ="krilin.png"
                },
                new Information
                {
                    Name="Piccolo",
                    Description="Piccolo is the son/reincarnation of the Namekian Piccolo Daimaō, and was spawned to avenge his sire's death at the hands of Goku. As time went on, he abandoned his ambitions to rule Earth and eventually joined Goku and the Z-Fighters as an ally and a friend. He seems to also share a deep bond with Goku's son Gohan, and appears to be a sort of second father to him. Piccolo has many signature moves such as the Special Beam Cannon, Hellzone Grenade, and Light Grenade",
                    imageUrl ="picollo.png"
                },
            };
        }
    }
}
